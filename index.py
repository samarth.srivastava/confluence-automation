from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import json
import pandas as pd
import time
import os
from generate_token import generate_token

# Setting path
jira_ticket="PG-7054"
url = 'https://easebuzz.atlassian.net/wiki/home'

# prefs = {
#     # 'printing.print_preview_sticky_settings.appState': json.dumps(settings),
#     'savefile.default_directory': '/home/easebuzz/Downloads/case_pdfs'
# }

def launchBrowser():
    driver_path = "/home/easebuzz/Downloads/chromedriver"
    email = "samarth.srivastava@easebuzz.in"
    password = "Samarth@09"

    # Open Website
    service = Service(executable_path=r'/usr/bin/chromedriver')
    options = webdriver.ChromeOptions()
    # options.add_argument('--disable-dev-shm-usage')
    options.add_argument("start-maximized")
    options.add_argument('--no-sandbox')
    options.add_argument("--kiosk-printing")
    # options.add_experimental_option('prefs', prefs)
    driver = webdriver.Chrome(service=service, options=options)
    driver.get(url)
    driver.maximize_window() # For maximizing window
    driver.implicitly_wait(5)

    # Validate ad Login
    email_element = driver.find_element(By.CLASS_NAME, "css-mfjdp3")
    pass_element = driver.find_element(By.ID, "password")
    submit_button_element = driver.find_element(By.CLASS_NAME, "css-1w9zxjf")
    email_element.send_keys(email)
    submit_button_element.click()
    pass_element.send_keys(password)
    submit_button_element.click()
    time.sleep(7.5)
    payment_gateway_section_element = driver.find_element(By.CLASS_NAME, "e1rzox6r1")
    payment_gateway_section_element.click()
    time.sleep(6)
    pg_releases_element = driver.find_element(By.CLASS_NAME, "css-1oonzef")
    pg_releases_element.click()
    time.sleep(6.5)
    default_template_selection_element = driver.find_element(By.XPATH, '//*[@id="editorContextPanelContentCreationTabs-0-tab"]/div/div[2]/div/div[4]/div/div[1]/div/button/div[2]/span[1]/div')
    default_template_selection_element.click()
    time.sleep(5)
    # driver.execute_script("window.open('about:blank', '_blank');")
    # window_handles = driver.window_handles
    # driver.switch_to.window(window_handles[1])
    # driver.get(jira_ticket)


    # GET Ticket details
    ticket_details = generate_token(jira_ticket)

    image_overrider_element = driver.find_element(By.CLASS_NAME, "css-1q3xxv2")
    image_overrider_element.click()

    # Switch back to the first window (index 0)
    # driver.switch_to.window(window_handles[0])


    """
    First Table Initial Details
    """
    # Fill Title of the Confluence Page
    title_element = driver.find_element(By.XPATH, '//*[@id="confluence-ui"]/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[2]/div[2]/div[2]/textarea')
    driver.execute_script("arguments[0].innerText = arguments[1];", title_element, ticket_details['title'])

    # Fill Release Version of the Ticket
    release_version_element = driver.find_element(By.XPATH, '//h4[text()="Product_Release_Version"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", release_version_element, ticket_details['fix_version'])

    # Fill Product Team of the Page
    product_team_element = driver.find_element(By.XPATH, '//h4[text()="Payment Solution/ NeoBanking/ Payment Gateway"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", product_team_element, ticket_details['project_name'])

    # Fill Product Module of the Page
    product_module_element = driver.find_element(By.XPATH, '//h4[text()="Eg. Instacollect, Smart Pay, EDU"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", product_module_element, ticket_details['impacted_areas'])

    # Fill Date of the Page
    date_element = driver.find_element(By.XPATH, '//h4[text()="DD/MM/YY"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", date_element, ticket_details['actual_deployment_date'])

    # Fill JIRA Ticket of the Page
    # ticket_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[1]/div/div[4]/table/tbody/tr[5]/td[2]/p')
    # driver.execute_script("arguments[0].innerHTML = arguments[1];", ticket_element, ticket_details['ticket_link'])

    # Fill Dev POC of the Page
    # devpoc_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[1]/div/div[4]/table/tbody/tr[6]/td[2]/p')
    # driver.execute_script("arguments[0].innerHTML = arguments[1];", devpoc_element, "@" + ticket_details['developer_poc'])

    # Fill QA POC of the Page
    # qapoc_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[1]/div/div[4]/table/tbody/tr[7]/td[2]/p')
    # driver.execute_script("arguments[0].innerHTML = arguments[1];", qapoc_element, ticket_details['qa_poc'])


    """
    Second Table Details
    """
    # Fill Product Column
    code_level_product_element = driver.find_element(By.XPATH, '//p[text()="Mention the Product vertical"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", code_level_product_element, ticket_details['project_name'])

    # Fill Module Column
    code_level_module_element = driver.find_element(By.XPATH, '//p[text()="Mention the modules/sub-modules selected for enhancement"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", code_level_module_element, ticket_details['impacted_areas'])

    # Fill Prev Behaviour Column
    prev_behav_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[3]/div/div[4]/table/tbody/tr[2]/td[3]/ul')
    driver.execute_script("arguments[0].innerHTML = arguments[1];", prev_behav_element, ticket_details['prev_behaviour'])

    # Fill Enhancements Made Column
    after_behav_element = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[2]/div/div[4]/table/tbody/tr[2]/td[4]')
    driver.execute_script("arguments[0].innerHTML = arguments[1];", after_behav_element, ticket_details['after_behaviour'])

    time.sleep(20)
    driver.__exit__()


    """
    Other Details
    """
    # Fill Rollback Plan Details
    # rollback_plan_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[31]")
    # driver.execute_script("arguments[0].innerHTML = arguments[1];", rollback_plan_element, "*Frontend:*\nRevert the commit to the old build\nPull it back on the server\n*Backend:*\nRevert master to the previous commit\nPull it back on the server\nRestart_nginx_uwsgi")

    # Fill DB Changes Details
    prereq_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[5]")
    driver.execute_script("arguments[0].innerText = arguments[1];", prereq_element, f"o   The user has an active account with Easebuzz\no   The user has {ticket_details['project_name']} product enabled for his account\no   The user has {ticket_details['impacted_areas']} feature enabled in {ticket_details['project_name']}, and visible on the dashboard.")

    # Fill DB Changes Details
    db_changes_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[9]")
    driver.execute_script("arguments[0].innerText = arguments[1];", db_changes_element, "NA")

    # Fill Deployment Steps Details
    deployment_steps_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[29]")
    driver.execute_script("arguments[0].innerText = arguments[1];", deployment_steps_element, "*Frontend:*\nUpdate the prod build into the respective branch.\nMerge PR for dashboard client changes into the master branch.\nCreate build using Command - npm run-script build-prod\nTake a pull on the server.\n*Backend →Dashboard:*\nMerge the PR for dashboard changes into the master branch.\nTake a pull on all the servers.\nRestart the application.\nPerform the check using the command - python manage.py check")

    # Fill additional docs Details
    additional_docs_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[27]")
    driver.execute_script("arguments[0].innerText = arguments[1];", additional_docs_element, ticket_details['attachments'])

    # Fill Android/iOS changes
    android_ios_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[25]")
    driver.execute_script("arguments[0].innerText = arguments[1];", android_ios_element, "NA")
   
    # Fill Android/iOS checks
    android_ios_check_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[23]")
    driver.execute_script("arguments[0].innerText = arguments[1];", android_ios_check_element, "NA")

    # Fill URL to test changes
    url_to_test_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[21]")
    driver.execute_script("arguments[0].innerText = arguments[1];", url_to_test_element, "NA")

    # Fill UI/UX Impact Details
    ui_ux_impact_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[19]")
    driver.execute_script("arguments[0].innerText = arguments[1];", ui_ux_impact_element, "NA")

    # Fill External Webhook Details
    external_webhook_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[17]")
    driver.execute_script("arguments[0].innerText = arguments[1];", external_webhook_element, "NA")

    # Fill Internal Webhook Details
    internal_webhook_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[15]")
    driver.execute_script("arguments[0].innerText = arguments[1];", internal_webhook_element, "NA")

    # Fill External Webhook Details
    external_api_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[13]")
    driver.execute_script("arguments[0].innerText = arguments[1];", external_api_element, "NA")
   
    # Fill Internal Webhook Details
    internal_api_element = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div/div[1]/div/div[1]/div[3]/div[3]/div[2]/div[2]/div[1]/div/div/div/div[5]/div[11]")
    driver.execute_script("arguments[0].innerText = arguments[1];", internal_api_element, "NA")

    # Fill Description of the Page
    desc_element = driver.find_element(By.XPATH, '//p[text()="e.g. Forgot password—Explain the use of this feature"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", desc_element, ticket_details['description'])



    """
    Approvals Table Details
    """
    vertical_element = driver.find_element(By.XPATH, '//strong[text()="<Vertical Name>"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", vertical_element, ticket_details['project_name'])
    lead_name_element = driver.find_element(By.XPATH, '//p[text()="<Lead Name>"]')
    driver.execute_script("arguments[0].innerText = arguments[1];", lead_name_element, ticket_details['lead_name'])



    time.sleep(30)

launchBrowser()
