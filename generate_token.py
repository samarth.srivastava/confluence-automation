import requests
import re

def generate_token(ticket):
    base_url = f'https://easebuzz.atlassian.net/rest/api/2/issue/{ticket}'
    
    # Jira username and API token (replace with your credentials)
    username = "samarth.srivastava@easebuzz.in"
    api_token = "ATATT3xFfGF0hpzReu5IzqoZExyTpb-A2P0CkkHxqibphBCQBqyzwfapdxWSvo1owMhouE9TBTUaB04JV6uP0McPnnkK2Bktv-WSbRHWOuhBz7weXb-_OTYxM76A9LPpwX9yhwY1lyHYB1YPPABGNNFzpLWw2hO7tOZG6qb4bkEMmYDIDwVIHL8=8997DB1C"

    # Create Basic Authentication header
    auth = (username, api_token)

    # Make the GET request to fetch issue details
    response = requests.get(base_url, auth=auth, verify=False)

    # Check if the request was successful (HTTP status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        issue_data = response.json()

    # Multiple Attachments
    attachment_list = []
    attachments = issue_data['fields']['attachment']
    if len(attachments) > 0:
        for data in attachments:
            attachment_list.append(data['content'])

    
    required_data = {
        'title': issue_data['fields']['summary'],
        'description': issue_data['fields'].get('description', ''),
        'project_key': issue_data['fields']['project']['key'],
        'project_name': issue_data['fields']['project']['name'],
        'impacted_areas': issue_data['fields'].get('customfield_10147'),
        'ticket_link': f"https://easebuzz.atlassian.net/browse/{ticket}",
        'developer_poc': issue_data['fields']['assignee']['displayName'],
        'qa_poc': issue_data['fields']['customfield_10151']['displayName'] if issue_data['fields']['customfield_10151'] else "",
        'fix_version': issue_data['fields']['fixVersions'][0]['name'] if len(issue_data['fields']['fixVersions']) is not 0 else "",
        'actual_deployment_date': issue_data['fields']['customfield_10175'] or issue_data['fields']['customfield_10171'],
        'lead_name': "Karan Manshani/Ranjeet Singh Raghuwanshi",
        'attachments': attachment_list if len(attachment_list) > 0 else "NA",
        'prev_behaviour': re.search(r"\*Problem Statement:\*\n\n(.*?)\n\n\*Solution:\*", issue_data['fields'].get('description'), re.DOTALL).group(1).strip(),
        'after_behaviour': issue_data['fields']['description'][issue_data['fields']['description'].find("*Solution:") + len("*Solution:"):].strip()
    }
    return required_data